package GETSKILLS;

import cucumber.api.java.After;
import cucumber.api.java.Before;

import java.io.FileNotFoundException;

/**
 * Created by Murali on 27/10/2016.
 */
public class Hooks {

    @Before
    public void SetUp()
    {
        try {
            new TestManager().OpenBrowser();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    @After
    public void TearDown()
    {
        new TestManager().CloseBrowser();

    }


}
