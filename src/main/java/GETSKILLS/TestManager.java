package GETSKILLS;



import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;



/**
 * Created by Murali on 27/10/2016.
 */

// public static Properties object = new Properties();

public class TestManager {


    public static Properties config = new Properties();
    public WebDriver driver = null;
    public FileInputStream fis;



    public void OpenBrowser() throws FileNotFoundException {
        if (driver == null) {

            //Loading config property file
             fis = new FileInputStream("src/test/Resources/config.properties");
            try {
                config.load(fis);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (config.getProperty("browser").equals("firefox")) {
                driver = new FirefoxDriver();
                driver.get(config.getProperty("url"));

            }
            else {

                System.out.println("inside user_is_on_Browser" + driver);

            }

            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
      // LoginXpath Xpath= PageFactory.initElements(driver,LoginXPath.class);


    }
    public void CloseBrowser()

    {
       // driver.quit();
    }



}
